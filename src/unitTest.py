import unittest
from main import *


class testingMethod(unittest.TestCase):

    def test_add(self):
        self.assertEqual(add(2,2), 4)

    def test_sub(self):
        self.assertEqual(sub(8,3), 5)

    def test_mul(self):
        self.assertEqual(mul(3,2), 6)

if __name__ == '__main__':
    unittest.main()